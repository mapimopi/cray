require "./structs"

lib LibRay
  # Font loading/unloading functions
  fun get_font_default = GetFontDefault : Font
  fun load_font = LoadFont(filename : LibC::Char*) : Font
  fun load_font_ex = LoadFontEx(filename : LibC::Char*, font_size : LibC::Int, num_chars : LibC::Int, font_chars : LibC::Int*) : Font
  fun load_font_data = LoadFontData(fileName : LibC::Char*, fontSize : LibC::Int, fontChars : LibC::Int*, charsCount : LibC::Int, sdf : Bool) : CharInfo*            # Load font data for further use
  fun gen_image_font_atlas = GenImageFontAtlas(chars : CharInfo*, fontSize : LibC::Int, charsCount : LibC::Int, padding : LibC::Int, packMethod : LibC::Int) : Image # Generate image font atlas using chars info
  fun unload_font = UnloadFont(font : Font) : Void

  # text drawing functions
  fun draw_fps = DrawFPS(pos_x : LibC::Int, pos_y : LibC::Int) : Void
  fun draw_text = DrawText(text : LibC::Char*, pos_x : LibC::Int, pos_y : LibC::Int, font_size : LibC::Int, color : Color) : Void
  fun draw_text_ex = DrawTextEx(font : Font, text : LibC::Char*, position : Vector2, font_size : LibC::Float, spacing : LibC::Int, color : Color) : Void

  # text misc. functions
  fun measure_text = MeasureText(text : LibC::Char*, font_size : LibC::Int) : LibC::Int                                        # measure string width for default font
  fun measure_text_ex = MeasureTextEx(font : Font, text : LibC::Char*, font_size : LibC::Float, spacing : LibC::Int) : Vector2 # measure string size for Font
  fun format_text = FormatText(text : LibC::Char*, ...) : LibC::Char*
  fun sub_text = SubText(text : LibC::Char*, position : LibC::Int, length : LibC::Int) : LibC::Char*
end
